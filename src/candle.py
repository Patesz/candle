import copy
from typing import List, Dict


HA_CLOSE_PRICE = 'ha_close_price'
HA_OPEN_PRICE = 'ha_open_price'
HA_HIGH_PRICE = 'ha_high_price'
HA_LOW_PRICE = 'ha_low_price'

_SHORT_TO_LONG_VALUE = {
    't': 'open_time',
    'T': 'close_time',
    'o': 'open_price',
    'h': 'high_price',
    'l': 'low_price',
    'c': 'close_price',
    'v': 'volume',
    'n': 'number_of_trades',
    'x': 'is_closed',
    'q': 'quote_asset_volume',
    'V': 'taker_buy_volume',
    'Q': 'taker_buy_quote_asset_volume'
}


class Candle:

    __slots__ = ('open_time', 'open_price', 'high_price', 'low_price', 'close_price', 'volume', 'close_time',
                 'number_of_trades', 'is_closed', 'quote_asset_volume', 'taker_buy_volume',
                 'taker_buy_quote_asset_volume')

    def __init__(self, open_time, open_price, high_price, low_price, close_price, volume, close_time=None,
                 number_of_trades=None, is_closed=True, quote_asset_volume=None, taker_buy_volume=None,
                 taker_buy_quote_asset_volume=None):
        self.open_time = int(open_time)
        self.open_price = float(open_price)
        self.high_price = float(high_price)
        self.low_price = float(low_price)
        self.close_price = float(close_price)
        self.volume = float(volume)
        self.is_closed = bool(is_closed)

        self.number_of_trades = None
        if number_of_trades is not None:
            self.number_of_trades = int(number_of_trades)

        self.close_time = None
        if close_time is not None:
            self.close_time = int(close_time)

        self.quote_asset_volume = None
        if quote_asset_volume is not None:
            self.quote_asset_volume = float(quote_asset_volume)

        self.taker_buy_volume = None
        if taker_buy_volume is not None:
            self.taker_buy_volume = float(taker_buy_volume)

        self.taker_buy_quote_asset_volume = None
        if taker_buy_quote_asset_volume is not None:
            self.taker_buy_quote_asset_volume = float(taker_buy_quote_asset_volume)

    def __gt__(self, other):
        if isinstance(other, Candle):
            return self.high_price - self.low_price > other.high_price - other.low_price
        return False

    def __ge__(self, other):
        if isinstance(other, Candle):
            return self.high_price - self.low_price >= other.high_price - other.low_price
        return False

    def __lt__(self, other):
        if isinstance(other, Candle):
            return self.high_price - self.low_price < other.high_price - other.low_price
        return False

    def __le__(self, other):
        if isinstance(other, Candle):
            return self.high_price - self.low_price <= other.high_price - other.low_price
        return False

    @classmethod
    def binance_historical(cls, data: List):
        """
        :param data: Order should be the following:
        :return:
        """
        return cls(
            open_time=data[0], open_price=data[1], high_price=data[2], low_price=data[3], close_price=data[4],
            volume=data[5], close_time=data[6], quote_asset_volume=data[7], number_of_trades=data[8],
            taker_buy_volume=data[9], taker_buy_quote_asset_volume=data[10]
        )

    @classmethod
    def binance_stream(cls, data: Dict):
        data = data['k']

        return cls(
            open_time=data['t'], open_price=data['o'], high_price=data['h'], low_price=data['l'], close_price=data['c'],
            volume=data['v'], close_time=data['T'], number_of_trades=data['n'], is_closed=data['x'],
            quote_asset_volume=data['q'], taker_buy_volume=data['V'], taker_buy_quote_asset_volume=data['Q']
        )

    def to_heikin_ashi(self) -> 'Candle':
        ha_candle = copy.deepcopy(self)
        ha_candle.close_price = (self.open_price + self.high_price + self.low_price + self.close_price) / 4
        ha_candle.open_price = (self.open_price + self.close_price) / 2
        return ha_candle

    def next_heikin_ashi(self, new: 'Candle') -> 'Candle':
        """
        self should already be a heikin ashi candle (if it is not, call to_heiken_ashi)

        :param new: Latest/current japanese candle.
        :return: Next heikin ashi candle.
        """

        ha = copy.deepcopy(new)

        ha.close_price = (new.open_price + new.high_price + new.low_price + new.close_price) / 4
        ha.open_price = (self.open_price + self.close_price) / 2
        ha.high_price = max(new.high_price, ha.open_price, ha.close_price)
        ha.low_price = min(new.low_price, ha.open_price, ha.close_price)

        return ha

    def is_strong(self) -> bool:
        """
        True if low price equals with open price, False otherwise
        """
        if self.is_bullish():
            return self.low_price == self.open_price
        return self.high_price == self.open_price

    def __len__(self):
        """ Returns the duration of the candle (close_time - open_time)"""
        return self.close_time - self.open_time

    def body_size(self) -> float:
        """ Returns the candles length (high - low) """
        return self.high_price - self.low_price

    def top_wick(self) -> float:
        if self.is_bullish():
            return self.high_price - self.close_price
        else:
            return self.high_price - self.open_price

    def bottom_wick(self) -> float:
        if self.is_bullish():
            return self.open_price - self.low_price
        else:
            return self.close_price - self.low_price

    def is_bullish(self) -> bool:
        return self.close_price > self.open_price

    def is_bearish(self) -> bool:
        return self.close_price < self.open_price

    # Do not make this method static!
    def to_list(self, values='tohlcv'):
        """
        :param values: Determines which fields to include in the Series.
        For example 'ohlcv' will include open time, open, high, low, close price and volume (order matters).

        Symbols are the following:
            * t - open time
            * T - close time
            * o - open price
            * h - high price
            * l - low price
            * c - close price
            * v - volume
            * n - number of trades
            * x - is closed
            * q - quote asset volume
            * V - taker buy (base asset) volume
            * Q - taker buy quote asset volume

        :return: A list with the specified values.
        """

        sf = locals()['self']
        fields = [_SHORT_TO_LONG_VALUE[value] for value in values]
        return [getattr(sf, field) for field in fields]

    # Do not make this method static!
    def to_dict(self, values='tohlcv'):
        """
        :param values: Determines which fields to include in the Series.
        For example 'ohlcv' will include open time, open, high, low, close price and volume (order matters).

        Symbols are the following:
            * t - open time
            * T - close time
            * o - open price
            * h - high price
            * l - low price
            * c - close price
            * v - volume
            * n - number of trades
            * x - is closed
            * q - quote asset volume
            * V - taker buy (base asset) volume
            * Q - taker buy quote asset volume

        :return: A dictionary with the specified values.
        """

        sf = locals()['self']
        fields = [_SHORT_TO_LONG_VALUE[value] for value in values]
        return {field: getattr(sf, field) for field in fields}

    def __str__(self):
        return f'Candle: (open_time={self.open_time}, open_price={self.open_price}, high_price={self.high_price}, ' \
               f'low_price={self.low_price}, close_price={self.close_price}, volume={self.volume} ' \
               f'close_time={self.close_time}, number_of_trades={self.number_of_trades}, is_closed={self.is_closed}, ' \
               f'quote_asset_volume={self.quote_asset_volume}, taker_buy_volume={self.taker_buy_volume}, ' \
               f'taker_buy_quote_asset_volume={self.taker_buy_quote_asset_volume})'
