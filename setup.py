from setuptools import setup

setup(
    name='candle',
    version='0.1.0',
    description='Python library which helps you create and manage time intervals.',
    py_modules=['candle'],
    package_dir={'': 'src'},
    url='https://gitlab.com/Patesz/candle',
    author='Ricky',
    author_email='p.ricky.dev@gmail.com',
    license='MIT',
    test_suite='tests'
)
