import unittest
from src.candle import Candle

# How to get data? -> client.get_historical_klines('BTCUSDT', '1M', 0, limit=1000)
historical_data = [1501545600000, '4261.48000000', '4745.42000000', '3400.00000000', '4724.89000000', '10015.64027200',
                   1504223999999, '42538297.66482722', 69180, '4610.01943100', '19419232.11660334', '10779.83873125']

stream_data = {
  "e": "kline",         # Event type
  "E": 123456789,       # Event time
  "s": "BTCUSDT",       # Symbol
  "k": {
    "t": 123400000,     # Kline start time
    "T": 123460000,     # Kline close time
    "s": "BTCUSDT",     # Symbol
    "i": "1m",          # Interval
    "f": 100,           # First trade ID
    "L": 200,           # Last trade ID
    "o": "1000.45",     # Open price
    "c": "900.61",      # Close price
    "h": "1021.40",     # High price
    "l": "890.62",      # Low price
    "v": "1000",        # Base asset volume
    "n": 100,           # Number of trades
    "x": False,         # Is this kline closed?
    "q": "1.0000",      # Quote asset volume
    "V": "500",         # Taker buy base asset volume
    "Q": "0.500",       # Taker buy quote asset volume
    "B": "123456"       # Ignore
  }
}

stream_data2 = {
  "e": "kline",         # Event type
  "E": 123456789,       # Event time
  "s": "BTCUSDT",       # Symbol
  "k": {
    "t": 123400000,     # Kline start time
    "T": 123460000,     # Kline close time
    "s": "BTCUSDT",     # Symbol
    "i": "1m",          # Interval
    "f": 100,           # First trade ID
    "L": 200,           # Last trade ID
    "o": "900.61",      # Open price
    "c": "892.15",      # Close price
    "h": "910.96",      # High price
    "l": "881.60",      # Low price
    "v": "1000",        # Base asset volume
    "n": 100,           # Number of trades
    "x": False,         # Is this kline closed?
    "q": "1.0000",      # Quote asset volume
    "V": "500",         # Taker buy base asset volume
    "Q": "0.500",       # Taker buy quote asset volume
    "B": "123456"       # Ignore
  }
}


class TestKline(unittest.TestCase):

    def setUp(self) -> None:
        self.historical1 = Candle.binance_historical(historical_data)
        self.stream1 = Candle.binance_stream(stream_data)
        self.stream2 = Candle.binance_stream(stream_data2)

    def test_historical(self):
        self.assertEqual(self.historical1.open_price, 4261.48)
        self.assertEqual(self.historical1.open_price, 4261.48)
        self.assertEqual(self.historical1.number_of_trades, 69180)
        self.assertEqual(self.historical1.body_size(), 1345.42)
        self.assertEqual(len(self.historical1), 2678399999)
        self.assertFalse(self.historical1.is_bearish())

    def test_stream(self):
        self.assertEqual(self.stream1.close_price, 900.61)
        self.assertEqual(self.stream1.open_price, 1000.45)
        self.assertEqual(self.stream1.high_price, 1021.40)
        self.assertEqual(self.stream1.low_price, 890.62)

    def test_heikin_ashi(self):
        ha_candle1 = self.stream1.to_heikin_ashi()
        self.assertEqual(ha_candle1.close_price, 953.27)
        self.assertEqual(ha_candle1.open_price, 950.53)
        self.assertEqual(ha_candle1.high_price, 1021.40)
        self.assertEqual(ha_candle1.low_price, 890.62)

        ha_candle2 = ha_candle1.next_heikin_ashi(self.stream2)
        self.assertEqual(ha_candle2.close_price, 896.33)
        self.assertEqual(ha_candle2.open_price, 951.9)
        self.assertEqual(ha_candle2.high_price, 951.9)
        self.assertEqual(ha_candle2.low_price, 881.60)

    def test_compare(self):
        self.assertTrue(self.stream1 > self.stream2)
        self.assertTrue(self.stream1 >= self.stream2)
        self.assertFalse(self.stream1 < self.stream2)
        self.assertFalse(self.stream1 <= self.stream2)
